package services;

import models.Users;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UsersServices {

    private static UsersServices instance;
    private Map<Integer,Users> users = new HashMap<>();

    public static UsersServices getInstance(){
        if (instance == null){
            instance = new UsersServices();
        }
        return instance;
    }
    public Set<Users> getAllUsers(){
        return new HashSet<>(users.values());
    }
}
