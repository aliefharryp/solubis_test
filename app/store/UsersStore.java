package store;

import models.Users;

import java.lang.reflect.Array;
import java.util.*;

public class UsersStore {
    private Map<Integer, Users> userss = new HashMap<>();

    public Set<Users> getAllUsers(){
        return new HashSet<>(userss.values());
    }

    public Optional<Users> addUsers(Users users) {
        int id = userss.size();
        users.setId(id);
        userss.put(id, users);
        return Optional.ofNullable((users));
    }

    public Optional<Users> getUsers(int userid){
        return Optional.ofNullable(userss.get(userid));
    }

    public Optional<Users> getUserByName(String username){
        return Optional.ofNullable(userss.get(username));
    }

    // public Optional<Users> cekUsers(Users users){
        
    //     // String[] du = userss.get(users.username);
    //     DataUser[] = 
    //     System.out.println(userss.get("admin1"));
    //     return Optional.ofNullable(userss.get(1));
    // }
}
