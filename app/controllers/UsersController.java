package controllers;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Users;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import store.UsersStore;
import utils.ApplicationUtil;

import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;
import java.util.List;
// import java.awt.List;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UsersController extends Controller {
    private HttpExecutionContext ec;
    private UsersStore usersStore;

    @Inject
    public UsersController (HttpExecutionContext ec, UsersStore usersStore){
        this.ec = ec;
        this.usersStore = usersStore;
    }

    public CompletionStage<Result> createUsers(Http.Request request){
        JsonNode json = request.body().asJson();
        return supplyAsync(() -> {
            if (json == null){
                return badRequest(ApplicationUtil.createResponse("Data is null", false));
            }
            Optional<Users> uOptional = usersStore.addUsers(Json.fromJson(json, Users.class));
            return uOptional.map(users -> {
                JsonNode jsonObject = Json.toJson(users);
                return created(ApplicationUtil.createResponse(jsonObject, true));
            }).orElse(internalServerError(ApplicationUtil.createResponse("Can't create data", false)));
        }, ec.current());
    }

    public CompletionStage<Result> listUsers(){
        return supplyAsync(() -> {
            Set<Users> result = usersStore.getAllUsers(); 
            System.out.println("You entered:" + result );       
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonData = mapper.convertValue(result, JsonNode.class);
            return ok(ApplicationUtil.createResponse(jsonData, true));
        }, ec.current());
    }

    public CompletionStage<Result> getUsersId(int userid){
        return supplyAsync(() -> {
            final Optional<Users> uOptional = usersStore.getUsers(userid);
            return uOptional.map(users -> {
                JsonNode jsonObject = Json.toJson(users);
                return ok(ApplicationUtil.createResponse(jsonObject, true));
            }).orElse(notFound(ApplicationUtil.createResponse("User Id " + userid + " is not found", false)));
        }, ec.current());
    }

    public CompletionStage<Result> loginUsers(Http.Request request){
        JsonNode json = request.body().asJson();
        
        // if (json == null){
        //     return badRequest(ApplicationUtil.createResponse("Data is null", false));
        // }
        String username = json.findPath("username").textValue();
        String password = json.findPath("password").textValue();

        List<Users> users = Users.find.where().eq("username",username).eq("password",password).findUnique();
        if(users != null){
            String response = "Success login";
        }else{
            String response = "Check your username & password";

        }

        System.out.println(users);

        return supplyAsync(() -> {
            final Optional<Users> uOptional = usersStore.getUserByName(username);
            return uOptional.map(users -> {
                JsonNode jsonObject = Json.toJson(users);
                return ok(ApplicationUtil.createResponse(jsonObject, true));
            }).orElse(notFound(ApplicationUtil.createResponse("User Id " + username + " is not found", false)));
        }, ec.current());


    }
    
}
