package models;

import javax.persistence.Entity;

@Entity
public class Users {
    private int userid;
    private String username;
    private String useremail;
    private String password;

    public Users(){

    }

    public Users(int userid, String username, String useremail, String password){
        this.userid = userid;
        this.username = username;
        this.useremail = useremail;
        this.password = password;
    }

    public int getId(){
        return userid;
    }
    public String getUsername(){
        return username;
    }
    public String getUseremail(){
        return useremail;
    }
    public String getPassword(){
        return password;
    }
    public void setId(int userid) {
        this.userid = userid;
    }

    // public Users getUsername1(String username){
    //     return find.where().eq("username",username).findUnique();
    // }

}


