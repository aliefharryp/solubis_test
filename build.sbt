name := """test_solubis2"""
organization := "ahpteams"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.10"


libraryDependencies ++= Seq(
    guice,
    javaJdbc, 
    javaWs,
    "mysql" % "mysql-connector-java" % "5.1.41",
    "org.avaje" % "ebean" % "2.7.3",
    "javax.persistence" % "persistence-api" % "1.0.2"
)

val appDependencies = Seq(
  "com.feth" %% "play-authenticate" % "0.9.0"
)


